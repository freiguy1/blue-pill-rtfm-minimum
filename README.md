# Blue Pill RTFM Minimum Program

This project is a starting point for running Rust on a [Blue Pill](https://wiki.stm32duino.com/index.php?title=Blue_Pill) with the real time framework [RTFM](https://github.com/japaric/cortex-m-rtfm).

At the time of this writing, this repository is working with these versions of rust and cargo:

```
rustc 1.30.0-nightly (5c875d938 2018-09-24)
cargo 1.31.0-nightly (de314a8b2 2018-09-21)
```

Also the `Cargo.toml` file is currently using a version of RTFM that comes from a github PR branch which is a bit more up to date. That PR will eventually go away so I'll need to watch/update that.

This repo also includes a `.vscode` folder which has some configuration files that allows for in-editor debugging. [This blog]() was very helpful in getting that all setup.

To get everything compiling and working, a decent amount of setup is required, and here isn't the place for that. Refer to [The Embedded Rust Book](https://rust-embedded.github.io/book/) for up to date setup instructions.
